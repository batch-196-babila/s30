// Count Items price < 50 and supplied by Yellow

db.fruits.aggregate([
    {$match: {price:{$lt:50}, supplier:"Yellow Farms"}},
    {$count: "ItemsPriceLessThan50"}
])

// All Price < 30
db.fruits.aggregate([
    {$match: {price:{$lt:30}}},
    {$count: "ItemsPriceLessThan30"}
])

// Average price of YF
db.fruits.aggregate([
    {$match: {supplier:"Yellow Farms"}},
    {$group: {_id:"$supplier", avgPrice:{$avg:"$price"}}}
])

// Highest price by RFI
db.fruits.aggregate([
    {$match: {supplier:"Red Farms Inc."}},
    {$group: {_id:"Highest Price", maxPrice:{$max:"$price"}}}
])

// Lowest price by RFI
db.fruits.aggregate([
    {$match: {supplier:"Red Farms Inc."}},
    {$group: {_id:"Lowest Price", minPrice:{$min:"$price"}}}
])