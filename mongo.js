// Group by Supplier and add all stocks


// aggregations needs a form of info that is not visibilty availble from your documents
// done in 2-3 steps

/*
    $match - get document which satisfies the condition
    syntax: {$match: {field:value}}

    $group - group documents and create an analysis out of the grouped document
*/

// _id and totalStocks are field(columns) title

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id:"$supplier", totalStocks:{$sum:"$stocks"}}}
])


// if _id's valueis definite or given, it will return one $group
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id:null, totalStocks:{$sum:"$stocks"}}}
])


db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id:"$supplier", totalStocks:{$sum:"$stocks"}}}
])

db.fruits.aggregate([
    {$match: {supplier: "Red Farms Inc."}},
    {$group: {_id:"RedFarms", totalStocks:{$sum:"$stocks"}}}
])

// shows stocks from Yellow Farms that currently onSale

db.fruits.aggregate([

    // {$match: {$and[{supplier:"Yellow Farm"}, {onSale:true}]}}
    {$match: {supplier: "Yellow Farms", onSale:true}},
    {$group: {_id:"Yello Farm", totalStocks:{$sum:"$stocks"}}}
])

// $avg - operator use in $group stage

// get all avaeragge of stock on each supplier
db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:"$supplier", avgStock: {$avg:"$stocks"}}}
])

// $max - allows to get the highest value out of all the value in a given field per group

db.fruits.aggregate([
    {$match: {onSale:true}},
    {$group: {_id:"highestStockOnSale"}, maxStock: {$max: "$stocks"}}
])

db.fruits.aggregate([
    {$match: {onSale:true}},
    {$group: {_id:"highestStockOnSale"}, minStock: {$min: "$stocks"}}
])

db.fruits.aggregate([
    {$match: {onSale:true}},
    {$group: {_id:null, maxPrice: {$max: "$stocks"}}}
])

db.fruits.aggregate([
    {$match: {onSale:true}},
    {$group: {_id:"lowestPriceOnSale", minPrice: {$min: "$price"}}}
])

//  select lowest number of stocks for all item
// which price is lower than 50

db.fruits.aggregate([
    {$match: {price:{$lt:50}}},
    {$group: {_id:"minStockLessThan50", minPrice: {$min: "$stocks"}}}
])

// Other stsages

// $cound is stage added after $match stage to count all items that matches the criteria

db.fruits.aggregate([
    {$match: {onSale:true}},
    {$count: "itemsOnSale"} // result 4 - banna is false out of 5
]) 

db.fruits.aggregate([
    {$match: {price:{$lt:50}}},
    {$count: "itemsPriceLessThan50"}
])

// Number of items with stocks less than 50

db.fruits.aggregate([
    {$match: {stocks:{$lt:20}}}, // result 3
    {$count: "forRestock"}
])

// $out - save/output the results in a new collection
// note: this will overwrite the collections if it already exists

db.fruits.aggregate([
    {$match: {onSale:true}},
    {$group: {_id:"$supplier", totalStocks:{$sum:"$stocks"}}},
    {$out: "stocksPerSupplier"}
])

db.fruits.aggregate([
    {$match: {onSale:true}},
    {$group: {_id:"$supplier", totalStocks:{$sum:"$stocks"}}},
    {$out: "stocksPerSupplier"}
])

// overrides the existing stocksPerSupplier
db.fruits.aggregate([
    {$match: {onSale:true}},
    {$group: {_id:"$supplier", avgPrice:{$avg:"$price"}}},
    {$out: "stocksPerSupplier"}
])